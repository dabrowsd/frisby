var frisby = require('frisby');

frisby.create('get libgdx page')
	.get('https://libgdx.badlogicgames.com/')
	.expectStatus(200)
	.toss();