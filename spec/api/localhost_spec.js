var frisby = require('frisby');

frisby.create('GET Method')
          .get('http://127.0.0.1:3000/blobs')
          .expectStatus(200)
          .expectHeaderContains('content-type', 'text/html')
         .toss();

frisby.create('Open new bloobs page')
	.get('http://127.0.0.1:3000/blobs/new')
	.expectStatus(200)
	.expectHeaderContains('content-type', 'text/html')
         .toss();